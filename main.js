let data = `{
    "game_duration": 300,
    "initial_credits": 1984,
    "items": [
        "Dwimeryt",
        "Cynamon",
        "Nuka-Cola",
        "Złoto",
        "Unobtainium",
        "Proteańskie dyski",
        "Ziemniaki",
        "Lyrium",
        "Murkwie",
        "Woda"
    ],
    "planets": {
        "Alderaan": {
            "available_items": {
                "Cynamon": {
                    "available": 74,
                    "buy_price": 6,
                    "sell_price": 6
                },
                "Dwimeryt": {
                    "available": 42,
                    "buy_price": 12,
                    "sell_price": 11
                },
                "Nuka-Cola": {
                    "available": 34,
                    "buy_price": 13,
                    "sell_price": 12
                },
                "Proteańskie dyski": {
                    "available": 5,
                    "buy_price": 76,
                    "sell_price": 69
                },
                "Unobtainium": {
                    "available": 23,
                    "buy_price": 33,
                    "sell_price": 31
                },
                "Woda": {
                    "available": 22,
                    "buy_price": 19,
                    "sell_price": 18
                },
                "Ziemniaki": {
                    "available": 10,
                    "buy_price": 92,
                    "sell_price": 86
                },
                "Złoto": {
                    "available": 12,
                    "buy_price": 19,
                    "sell_price": 17
                }
            },
            "x": 15,
            "y": 32
        },
        "Argoland": {
            "available_items": {
                "Dwimeryt": {
                    "available": 23,
                    "buy_price": 10,
                    "sell_price": 10
                },
                "Lyrium": {
                    "available": 39,
                    "buy_price": 9,
                    "sell_price": 8
                },
                "Murkwie": {
                    "available": 5,
                    "buy_price": 73,
                    "sell_price": 64
                },
                "Nuka-Cola": {
                    "available": 25,
                    "buy_price": 22,
                    "sell_price": 19
                },
                "Proteańskie dyski": {
                    "available": 10,
                    "buy_price": 75,
                    "sell_price": 65
                },
                "Ziemniaki": {
                    "available": 6,
                    "buy_price": 69,
                    "sell_price": 61
                },
                "Złoto": {
                    "available": 12,
                    "buy_price": 34,
                    "sell_price": 30
                }
            },
            "x": 59,
            "y": 44
        },
        "Arrakis": {
            "available_items": {
                "Cynamon": {
                    "available": 59,
                    "buy_price": 8,
                    "sell_price": 7
                },
                "Lyrium": {
                    "available": 53,
                    "buy_price": 10,
                    "sell_price": 8
                },
                "Murkwie": {
                    "available": 6,
                    "buy_price": 89,
                    "sell_price": 76
                },
                "Nuka-Cola": {
                    "available": 25,
                    "buy_price": 16,
                    "sell_price": 15
                },
                "Proteańskie dyski": {
                    "available": 7,
                    "buy_price": 64,
                    "sell_price": 57
                },
                "Unobtainium": {
                    "available": 12,
                    "buy_price": 36,
                    "sell_price": 33
                },
                "Woda": {
                    "available": 12,
                    "buy_price": 25,
                    "sell_price": 21
                },
                "Ziemniaki": {
                    "available": 9,
                    "buy_price": 120,
                    "sell_price": 107
                },
                "Złoto": {
                    "available": 16,
                    "buy_price": 23,
                    "sell_price": 21
                }
            },
            "x": 81,
            "y": 34
        },
        "Corellia": {
            "available_items": {
                "Dwimeryt": {
                    "available": 38,
                    "buy_price": 8,
                    "sell_price": 8
                },
                "Lyrium": {
                    "available": 63,
                    "buy_price": 8,
                    "sell_price": 7
                },
                "Murkwie": {
                    "available": 6,
                    "buy_price": 91,
                    "sell_price": 84
                },
                "Proteańskie dyski": {
                    "available": 10,
                    "buy_price": 74,
                    "sell_price": 66
                },
                "Unobtainium": {
                    "available": 11,
                    "buy_price": 30,
                    "sell_price": 26
                },
                "Ziemniaki": {
                    "available": 12,
                    "buy_price": 71,
                    "sell_price": 66
                },
                "Złoto": {
                    "available": 19,
                    "buy_price": 37,
                    "sell_price": 33
                }
            },
            "x": 43,
            "y": 69
        },
        "Encja": {
            "available_items": {
                "Cynamon": {
                    "available": 59,
                    "buy_price": 6,
                    "sell_price": 5
                },
                "Dwimeryt": {
                    "available": 56,
                    "buy_price": 10,
                    "sell_price": 10
                },
                "Lyrium": {
                    "available": 51,
                    "buy_price": 9,
                    "sell_price": 8
                },
                "Murkwie": {
                    "available": 6,
                    "buy_price": 88,
                    "sell_price": 76
                },
                "Nuka-Cola": {
                    "available": 35,
                    "buy_price": 17,
                    "sell_price": 16
                },
                "Proteańskie dyski": {
                    "available": 9,
                    "buy_price": 103,
                    "sell_price": 90
                },
                "Unobtainium": {
                    "available": 13,
                    "buy_price": 39,
                    "sell_price": 37
                },
                "Woda": {
                    "available": 12,
                    "buy_price": 32,
                    "sell_price": 32
                },
                "Ziemniaki": {
                    "available": 6,
                    "buy_price": 60,
                    "sell_price": 57
                },
                "Złoto": {
                    "available": 26,
                    "buy_price": 40,
                    "sell_price": 35
                }
            },
            "x": 91,
            "y": 32
        },
        "Gaia": {
            "available_items": {
                "Cynamon": {
                    "available": 80,
                    "buy_price": 6,
                    "sell_price": 6
                },
                "Dwimeryt": {
                    "available": 85,
                    "buy_price": 8,
                    "sell_price": 7
                },
                "Lyrium": {
                    "available": 41,
                    "buy_price": 10,
                    "sell_price": 9
                },
                "Proteańskie dyski": {
                    "available": 9,
                    "buy_price": 102,
                    "sell_price": 94
                },
                "Woda": {
                    "available": 25,
                    "buy_price": 43,
                    "sell_price": 39
                },
                "Ziemniaki": {
                    "available": 8,
                    "buy_price": 92,
                    "sell_price": 82
                },
                "Złoto": {
                    "available": 16,
                    "buy_price": 35,
                    "sell_price": 31
                }
            },
            "x": 75,
            "y": 76
        },
        "Ksi": {
            "available_items": {
                "Cynamon": {
                    "available": 33,
                    "buy_price": 11,
                    "sell_price": 10
                },
                "Dwimeryt": {
                    "available": 80,
                    "buy_price": 6,
                    "sell_price": 6
                },
                "Lyrium": {
                    "available": 64,
                    "buy_price": 8,
                    "sell_price": 7
                },
                "Murkwie": {
                    "available": 4,
                    "buy_price": 73,
                    "sell_price": 67
                },
                "Nuka-Cola": {
                    "available": 30,
                    "buy_price": 17,
                    "sell_price": 14
                },
                "Proteańskie dyski": {
                    "available": 8,
                    "buy_price": 39,
                    "sell_price": 37
                },
                "Unobtainium": {
                    "available": 12,
                    "buy_price": 41,
                    "sell_price": 39
                },
                "Woda": {
                    "available": 15,
                    "buy_price": 30,
                    "sell_price": 28
                },
                "Ziemniaki": {
                    "available": 6,
                    "buy_price": 74,
                    "sell_price": 64
                },
                "Złoto": {
                    "available": 16,
                    "buy_price": 20,
                    "sell_price": 18
                }
            },
            "x": 91,
            "y": 71
        },
        "Leonida": {
            "available_items": {
                "Cynamon": {
                    "available": 36,
                    "buy_price": 12,
                    "sell_price": 11
                },
                "Dwimeryt": {
                    "available": 50,
                    "buy_price": 8,
                    "sell_price": 7
                },
                "Lyrium": {
                    "available": 60,
                    "buy_price": 9,
                    "sell_price": 9
                },
                "Murkwie": {
                    "available": 6,
                    "buy_price": 89,
                    "sell_price": 85
                },
                "Nuka-Cola": {
                    "available": 39,
                    "buy_price": 18,
                    "sell_price": 16
                },
                "Proteańskie dyski": {
                    "available": 7,
                    "buy_price": 65,
                    "sell_price": 57
                },
                "Unobtainium": {
                    "available": 9,
                    "buy_price": 38,
                    "sell_price": 33
                },
                "Ziemniaki": {
                    "available": 5,
                    "buy_price": 121,
                    "sell_price": 112
                },
                "Złoto": {
                    "available": 11,
                    "buy_price": 45,
                    "sell_price": 41
                }
            },
            "x": 32,
            "y": 5
        },
        "NowWhat": {
            "available_items": {
                "Cynamon": {
                    "available": 62,
                    "buy_price": 8,
                    "sell_price": 7
                },
                "Dwimeryt": {
                    "available": 22,
                    "buy_price": 9,
                    "sell_price": 9
                },
                "Murkwie": {
                    "available": 9,
                    "buy_price": 67,
                    "sell_price": 66
                },
                "Nuka-Cola": {
                    "available": 27,
                    "buy_price": 18,
                    "sell_price": 16
                },
                "Proteańskie dyski": {
                    "available": 9,
                    "buy_price": 82,
                    "sell_price": 71
                },
                "Ziemniaki": {
                    "available": 4,
                    "buy_price": 74,
                    "sell_price": 63
                },
                "Złoto": {
                    "available": 17,
                    "buy_price": 28,
                    "sell_price": 24
                }
            },
            "x": 35,
            "y": 41
        },
        "Sur'Kesh": {
            "available_items": {
                "Cynamon": {
                    "available": 55,
                    "buy_price": 9,
                    "sell_price": 8
                },
                "Lyrium": {
                    "available": 34,
                    "buy_price": 9,
                    "sell_price": 8
                },
                "Murkwie": {
                    "available": 10,
                    "buy_price": 73,
                    "sell_price": 66
                },
                "Nuka-Cola": {
                    "available": 30,
                    "buy_price": 19,
                    "sell_price": 17
                },
                "Proteańskie dyski": {
                    "available": 5,
                    "buy_price": 85,
                    "sell_price": 79
                },
                "Unobtainium": {
                    "available": 19,
                    "buy_price": 34,
                    "sell_price": 31
                },
                "Woda": {
                    "available": 21,
                    "buy_price": 23,
                    "sell_price": 20
                },
                "Ziemniaki": {
                    "available": 8,
                    "buy_price": 99,
                    "sell_price": 95
                }
            },
            "x": 39,
            "y": 31
        },
        "Tairia": {
            "available_items": {
                "Cynamon": {
                    "available": 70,
                    "buy_price": 10,
                    "sell_price": 10
                },
                "Lyrium": {
                    "available": 43,
                    "buy_price": 6,
                    "sell_price": 5
                },
                "Murkwie": {
                    "available": 8,
                    "buy_price": 97,
                    "sell_price": 84
                },
                "Nuka-Cola": {
                    "available": 32,
                    "buy_price": 20,
                    "sell_price": 19
                },
                "Unobtainium": {
                    "available": 19,
                    "buy_price": 44,
                    "sell_price": 41
                },
                "Woda": {
                    "available": 12,
                    "buy_price": 29,
                    "sell_price": 25
                },
                "Ziemniaki": {
                    "available": 6,
                    "buy_price": 123,
                    "sell_price": 103
                },
                "Złoto": {
                    "available": 14,
                    "buy_price": 37,
                    "sell_price": 34
                }
            },
            "x": 36,
            "y": 84
        },
        "Tatooine": {
            "available_items": {
                "Cynamon": {
                    "available": 60,
                    "buy_price": 11,
                    "sell_price": 10
                },
                "Dwimeryt": {
                    "available": 64,
                    "buy_price": 10,
                    "sell_price": 9
                },
                "Lyrium": {
                    "available": 45,
                    "buy_price": 8,
                    "sell_price": 7
                },
                "Murkwie": {
                    "available": 6,
                    "buy_price": 81,
                    "sell_price": 71
                },
                "Nuka-Cola": {
                    "available": 39,
                    "buy_price": 15,
                    "sell_price": 13
                },
                "Proteańskie dyski": {
                    "available": 7,
                    "buy_price": 89,
                    "sell_price": 84
                },
                "Unobtainium": {
                    "available": 13,
                    "buy_price": 37,
                    "sell_price": 32
                },
                "Woda": {
                    "available": 10,
                    "buy_price": 23,
                    "sell_price": 21
                },
                "Ziemniaki": {
                    "available": 7,
                    "buy_price": 95,
                    "sell_price": 87
                },
                "Złoto": {
                    "available": 19,
                    "buy_price": 35,
                    "sell_price": 32
                }
            },
            "x": 47,
            "y": 68
        },
        "Tuchanka": {
            "available_items": {
                "Cynamon": {
                    "available": 59,
                    "buy_price": 10,
                    "sell_price": 9
                },
                "Dwimeryt": {
                    "available": 51,
                    "buy_price": 7,
                    "sell_price": 6
                },
                "Lyrium": {
                    "available": 65,
                    "buy_price": 11,
                    "sell_price": 10
                },
                "Murkwie": {
                    "available": 9,
                    "buy_price": 90,
                    "sell_price": 82
                },
                "Nuka-Cola": {
                    "available": 46,
                    "buy_price": 18,
                    "sell_price": 16
                },
                "Proteańskie dyski": {
                    "available": 10,
                    "buy_price": 71,
                    "sell_price": 65
                },
                "Unobtainium": {
                    "available": 8,
                    "buy_price": 39,
                    "sell_price": 37
                },
                "Woda": {
                    "available": 15,
                    "buy_price": 28,
                    "sell_price": 24
                },
                "Ziemniaki": {
                    "available": 10,
                    "buy_price": 61,
                    "sell_price": 57
                },
                "Złoto": {
                    "available": 12,
                    "buy_price": 46,
                    "sell_price": 40
                }
            },
            "x": 27,
            "y": 76
        },
        "Ziemia": {
            "available_items": {
                "Cynamon": {
                    "available": 58,
                    "buy_price": 8,
                    "sell_price": 7
                },
                "Dwimeryt": {
                    "available": 106,
                    "buy_price": 8,
                    "sell_price": 7
                },
                "Lyrium": {
                    "available": 31,
                    "buy_price": 9,
                    "sell_price": 8
                },
                "Murkwie": {
                    "available": 7,
                    "buy_price": 82,
                    "sell_price": 75
                },
                "Nuka-Cola": {
                    "available": 30,
                    "buy_price": 18,
                    "sell_price": 17
                },
                "Unobtainium": {
                    "available": 21,
                    "buy_price": 37,
                    "sell_price": 36
                },
                "Ziemniaki": {
                    "available": 6,
                    "buy_price": 77,
                    "sell_price": 69
                },
                "Złoto": {
                    "available": 13,
                    "buy_price": 38,
                    "sell_price": 32
                }
            },
            "x": 94,
            "y": 24
        }
    },
    "starships": {
        "Axiom": {
            "cargo_hold_size": 27,
            "position": "Tatooine"
        },
        "Enterprise": {
            "cargo_hold_size": 46,
            "position": "Corellia"
        },
        "Goliath": {
            "cargo_hold_size": 33,
            "position": "Sur'Kesh"
        },
        "Hermes": {
            "cargo_hold_size": 26,
            "position": "NowWhat"
        },
        "Millenium Falcon": {
            "cargo_hold_size": 35,
            "position": "Tatooine"
        },
        "Niezwyciężony": {
            "cargo_hold_size": 60,
            "position": "Argoland"
        },
        "Normandy SR-2": {
            "cargo_hold_size": 40,
            "position": "Gaia"
        },
        "Nostromo": {
            "cargo_hold_size": 25,
            "position": "Arrakis"
        },
        "Rocinante": {
            "cargo_hold_size": 30,
            "position": "Alderaan"
        },
        "Космонавт Алексе́й Лео́нов":{
            "cargo_hold_size":	35,
            "position":	"Arrakis"
        }
    }
}`;
class Item {
    constructor(name, quantity) {
        this.name = name;
        this.quantity = quantity;
    }
    toStringDebug() {
        return this.name + " quantity: " + this.quantity;
    }
}
// tslint:disable-next-line: max-classes-per-file
class Item_trade {
    constructor(name, available, buy_price, sell_price) {
        this.name = name;
        this.available = available;
        this.buy_price = buy_price;
        this.sell_price = sell_price;
    }
    toStringDebug() {
        return this.name + " available: " + this.available + " buy: " + this.buy_price + " sell: " + this.sell_price;
    }
}
var Move;
(function (Move) {
    Move["On_planet"] = "on planet";
    Move["On_way"] = "on way";
})(Move || (Move = {}));
// tslint:disable-next-line: max-classes-per-file
class Ship {
    constructor(name, cargo_max_size, from, to) {
        this.name = name;
        this.cargo_max_size = cargo_max_size;
        this.cargo_size = 0;
        this.cargo = new Map();
        this.planet_from = from;
        this.planet_to = to;
        this.where = Move.On_planet;
    }
    toStringDebug() {
        let s = "";
        s += "name: " + this.name + "\n" + "cargo" + this.cargo_size + "/" + this.cargo_max_size + ":\n";
        // tslint:disable-next-line: prefer-for-of
        for (let k of this.cargo.keys()) {
            s += this.cargo.get(k).toStringDebug() + "\n";
        }
        s += "where:" + this.where + "\n";
        if (this.where === Move.On_planet) {
            s += this.planet_from;
        }
        else {
            s += "from : " + this.planet_from + " to: " + this.planet_to + "\n";
        }
        return s;
    }
    give(itemName, quantity) {
        if (this.cargo.has(itemName)) {
            this.cargo.get(itemName).quantity -= quantity;
        }
        if (this.cargo.get(itemName).quantity === 0) {
            this.cargo.delete(itemName);
        }
    }
    get(itemName, quantity) {
        if (!this.cargo.has(itemName)) {
            this.cargo.set(itemName, new Item(itemName, 0));
        }
        this.cargo.get(itemName).quantity += quantity;
    }
    getItemQuantity(itemName) {
        if (this.cargo.has(itemName)) {
            return this.cargo.get(itemName).quantity;
        }
        return 0;
    }
}
// tslint:disable-next-line: max-classes-per-file
class Planet {
    // tslint:disable-next-line: variable-name
    constructor(name, available_items, x, y) {
        this.name = name;
        this.items = new Map();
        this.ships = new Map();
        for (let i = 0; i < available_items.length; i++) {
            this.items.set(available_items[i].name, available_items[i]);
        }
        this.x = x;
        this.y = y;
    }
    toStringDebug() {
        let s = "";
        s += "Planet: " + this.name + "\n";
        // tslint:disable-next-line: prefer-for-of
        s += "items: \n";
        this.items.forEach((v, k, m) => {
            s += v.toStringDebug() + "\n";
        });
        s += "ships: \n";
        for (let k of this.ships.keys()) {
            s += k + "\n";
        }
        s += "x: " + this.x + "\n";
        s += "y: " + this.y + "\n";
        return s;
    }
    getCoordinates() {
        return [this.x, this.y];
    }
    getPriceSell(name) {
        if (this.items.has(name)) {
            return this.items.get(name).sell_price;
        }
        return 0;
    }
    getPriceBuy(name) {
        if (this.items.has(name)) {
            return this.items.get(name).buy_price;
        }
        return 0;
    }
    getItemQuantity(name) {
        if (this.items.has(name)) {
            return this.items.get(name).available;
        }
        return 0;
    }
    give(name, quantity) {
        this.items.get(name).available -= quantity;
    }
    get(name, quantity) {
        if (this.items.has(name)) {
            this.items.get(name).available += quantity;
            return;
        }
        this.items.set(name, new Item_trade(name, quantity, 0, 0));
    }
}
var View;
(function (View) {
    View[View["Planet"] = 0] = "Planet";
    View[View["Shit_travel"] = 1] = "Shit_travel";
    View[View["Ship_planet"] = 2] = "Ship_planet";
    View[View["Main"] = 3] = "Main";
})(View || (View = {}));
// tslint:disable-next-line: max-classes-per-file
class Game {
    static getTime() {
        const d = new Date(); // for now
        const h = d.getHours(); // => 9
        const m = d.getMinutes(); // =>  30
        const s = d.getSeconds(); // => 51
        return h * 60 * 60 + m * 60 + s;
    }
    constructor(data) {
        this.view = View.Main;
        const planetsToParse = data.planets;
        this.game_duration = data.game_duration;
        this.balance = data.initial_credits;
        this.items = data.items;
        this.planets = new Map();
        this.ships = new Map();
        this.nick = "";
        this.time = this.game_duration;
        // tslint:disable-next-line: forin
        for (let key in planetsToParse) {
            let value = planetsToParse[key];
            let items = [];
            // tslint:disable-next-line: forin
            for (const i in planetsToParse[key].available_items) {
                let v = planetsToParse[key].available_items[i];
                items.push(new Item_trade(i, v.available, v.buy_price, v.sell_price));
            }
            let x = this.planets.set(key, new Planet(key, items, value.x, value.y));
        }
        const shipsToParse = data.starships;
        // tslint:disable-next-line: forin
        for (let key in shipsToParse) {
            let value = shipsToParse[key];
            let p = value.position;
            let ship = new Ship(key, value.cargo_hold_size, p, p);
            let x = this.ships.set(key, ship);
            this.planets.get(value.position).ships.set(key, ship);
        }
        this.start_time = Game.getTime();
        this.now = this.start_time;
        this.end_time = this.start_time + this.game_duration;
    }
    toStringDebug() {
        let s = "";
        s += "game_duration: " + this.game_duration + "\n";
        s += "balance: " + this.balance + "\n";
        // tslint:disable-next-line: prefer-for-of
        for (let k of this.planets.keys()) {
            s += this.planets.get(k).toStringDebug() + "\n";
        }
        // tslint:disable-next-line: prefer-for-of
        for (let k of this.ships.keys()) {
            s += this.ships.get(k).toStringDebug() + "\n";
        }
        return s;
    }
}
let game;
function countDistance(a, b) {
    let dx = Math.abs(a.x - b.x);
    let dy = Math.abs(a.y - b.y);
    let way = Math.sqrt(dx * dx + dy * dy);
    return Math.round(way);
}
function updateFlyMain(name) {
    let tab = document.getElementById("ships_main");
    for (let i = 0; i < tab.rows.length; i++) {
        let row = tab.rows[i];
        if (row.cells[0].textContent === name) {
            row.cells[0].onclick = () => { goToShipWay(name); };
            row.cells[1].textContent = "w dordze";
            row.cells[1].onclick = function () { return false; };
            return;
        }
    }
}
function updateArivalMain(name, planet) {
    let tab = document.getElementById("ships_main");
    for (let i = 0; i < tab.rows.length; i++) {
        let row = tab.rows[i];
        if (row.cells[0].textContent === name) {
            row.cells[0].onclick = () => { goToShipPlanet(name, planet); };
            row.cells[1].textContent = planet;
            row.cells[1].onclick = () => { goToPlanet(planet); };
            return;
        }
    }
}
function updateShipArivalPlanet(planet, ship) {
    const table = document.getElementById("ships_planet");
    if (table != null) {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < table.rows.length; i++) {
            if (table.rows[i].cells[0].textContent == ship.name)
                return;
        }
        addShipToPlanet(table.insertRow(-1), ship);
    }
}
function arival(ship) {
    ship.moving = false;
    ship.planet_from = ship.planet_to;
    let planetWeArrived = game.planets.get(ship.planet_to);
    planetWeArrived.ships.set(ship.name, ship);
}
function arivalDisplay(ship) {
    updateArivalMain(ship.name, ship.planet_to);
    if (game.view === View.Shit_travel && game.ship.name === ship.name) {
        goToShipPlanet(ship.name, ship.planet_to);
        return;
    }
    if (game.view === View.Planet && game.planet.name === ship.planet_to) {
        updateShipArivalPlanet(game.planet, ship);
        return;
    }
}
function flyCountdownDisplay(time, ship) {
    let interval = setInterval(() => {
        if (time === 0) {
            clearInterval(interval);
            arivalDisplay(ship);
            return;
        }
        time = time - 1;
        if (game.ship.name === ship.name) {
            document.getElementById("time_way").textContent = time.toString(10);
        }
    }, 1000);
}
function flyCountdown(time, ship) {
    setTimeout(() => {
        arival(ship);
    }, time * 1000 + 50);
}
function flyTo(destination) {
    let ship = game.ship;
    ship.moving = true;
    let planet = game.planets.get(ship.planet_from);
    ship.planet_to = destination;
    let time = countDistance(game.planets.get(ship.planet_from), game.planets.get(ship.planet_to));
    planet.ships.delete(ship.name);
    flyCountdown(time, ship);
    return time;
}
function flyToWithDisplay() {
    let sel = document.getElementById("destination");
    const time = flyTo(sel.value);
    updateFlyMain(game.ship.name);
    flyCountdownDisplay(time, game.ship);
    goToMain();
}
function hideGridAll() {
    hideGrid("grid-container_planet");
    hideGrid("grid-container_way");
    hideGrid("grid-container_ship_planet");
}
function setNick() {
    document.getElementById("nick_main_nick").textContent = game.nick;
    document.getElementById("nick").textContent = game.nick;
    document.getElementsByClassName("nick_ship_planet")[0].textContent = game.nick;
    document.getElementsByClassName("nick_way")[0].textContent = game.nick;
}
function setBalance() {
    document.getElementById("balance_main").textContent = game.balance.toString(10) + "$";
    document.getElementById("score").textContent = game.balance.toString(10) + "$";
    document.getElementsByClassName("balance_way")[0].textContent = game.balance.toString(10) + "$";
    document.getElementsByClassName("balance_ship_planet")[0].textContent = game.balance.toString(10) + "$";
}
function setTime() {
    let interval = setInterval(() => {
        if (game.time === 0) {
            end_game(interval);
            return;
        }
        game.time = game.time - 1;
        document.getElementById("end_game_main").textContent = game.time.toString(10);
        document.getElementsByClassName("game_end_ship_planet")[0].textContent = game.time.toString(10);
        document.getElementsByClassName("game_end_way")[0].textContent = game.time.toString(10);
        document.getElementsByClassName("end_game_planet")[0].textContent = game.time.toString(10);
    }, 1000);
}
function setCargoShipWay() {
    let cargo = game.ship.cargo;
    clearTableId("cargo_way");
    let tableToAdd = getTab("cargo_way");
    for (let k of cargo.values()) {
        let row = tableToAdd.insertRow();
        addCell(row, k.name);
        addCell(row, k.quantity.toString());
    }
}
function setShipWay() {
    document.getElementById("ship_way").textContent = game.ship.name;
    document.getElementById("from_way").textContent = "POCZĄTEK: " + game.ship.planet_from;
    document.getElementById("to_way").textContent = "KONIEC: " + game.ship.planet_to;
    document.getElementById("capacity_way").textContent
        = "ŁADOWNIA: " + game.ship.cargo_size + "/" + game.ship.cargo_max_size;
    setCargoShipWay();
}
function goToShipWay(ship) {
    game.ship = game.ships.get(ship);
    game.view = View.Shit_travel;
    setShipWay();
    hideGridAll();
    showGrid("grid-container_way");
}
function goToShipPlanet(ship, planet) {
    game.view = View.Ship_planet;
    game.ship = game.ships.get(ship);
    game.planet = game.planets.get(planet);
    hideGridAll();
    showGrid("grid-container_ship_planet");
    setShipOnPlanet();
}
// PLANET
function addShipToPlanet(newRow, ship) {
    let newCell = newRow.insertCell();
    newCell.appendChild(document.createTextNode(ship.name));
    newCell.onclick = () => { goToShipPlanet(ship.name, ship.planet_from); };
}
function setShipsPlanet() {
    clearTableId("ships_planet");
    const table = document.getElementById("ships_planet");
    if (table != null) {
        // tslint:disable-next-line: prefer-for-of
        for (let s of game.planet.ships.keys()) {
            const ship = game.planet.ships.get(s);
            addShipToPlanet(table.insertRow(-1), ship);
        }
    }
}
function setCargoPlanet() {
    let p = game.planet.items;
    clearTableId("cargo_planet");
    let tableToAdd = getTab("cargo_planet");
    for (let k of p.values()) {
        let row = tableToAdd.insertRow();
        addCell(row, k.name);
        addCell(row, k.available.toString());
        addCell(row, k.buy_price.toString());
        addCell(row, k.sell_price.toString());
    }
}
function setPlanet() {
    document.getElementById("name_planet").textContent = game.planet.name;
    setBalance();
    setShipsPlanet();
    setCargoPlanet();
}
function goToPlanet(planet) {
    game.view = View.Planet;
    game.planet = game.planets.get(planet);
    setPlanet();
    hideGridAll();
    showGrid("grid-container_planet");
}
// ~PLANET~
// MIAN
function clearTableId(id) {
    const table = document.getElementById(id);
    while (table.rows.length > 2) {
        table.deleteRow(2);
    }
}
function clearTableClass(id) {
    const table = document.getElementsByClassName(id)[0];
    while (table.rows.length > 2) {
        table.deleteRow(2);
    }
}
function addShipToMain(newRow, ship) {
    let newCell = newRow.insertCell();
    newCell.appendChild(document.createTextNode(ship.name));
    if (ship.moving) {
        newCell.onclick = () => { goToShipWay(ship.name); };
    }
    else {
        newCell.onclick = () => { goToShipPlanet(ship.name, ship.planet_from); };
    }
    if (ship.moving) {
        newCell = newRow.insertCell();
        newCell.appendChild(document.createTextNode("w podrozy"));
    }
    else {
        newCell = newRow.insertCell();
        newCell.onclick = () => { goToPlanet(ship.planet_from); };
        newCell.appendChild(document.createTextNode(ship.planet_from));
    }
}
function setShipsMain() {
    clearTableId("ships_main");
    const table = document.getElementById("ships_main");
    if (table != null) {
        // tslint:disable-next-line: prefer-for-of
        for (let name of game.ships.keys()) {
            const ship = game.ships.get(name);
            addShipToMain(table.insertRow(-1), ship);
        }
    }
}
function addPlanetToMain(newRow, planet) {
    let newCell = newRow.insertCell();
    newCell.appendChild(document.createTextNode(planet.name));
    newCell.onclick = () => { goToPlanet(planet.name); };
    newCell.onclick = () => { goToPlanet(planet.name); };
    newCell = newRow.insertCell();
    const coordinate = planet.x + ", " + planet.y;
    newCell.appendChild(document.createTextNode(coordinate));
}
function setPlanetsMain() {
    clearTableId("planets_main");
    const table = document.getElementById("planets_main");
    if (table != null) {
        // tslint:disable-next-line: prefer-for-of
        for (let name of game.planets.keys()) {
            const planet = game.planets.get(name);
            addPlanetToMain(table.insertRow(-1), planet);
        }
    }
}
function hideGrid(s) {
    let div = document.getElementsByClassName(s)[0];
    div.style.display = "none";
}
function showGrid(s) {
    let div = document.getElementsByClassName(s)[0];
    div.style.display = "grid";
}
function init() {
    setTime();
    hideGrid("grid-container_start");
    hideGrid("grid-container_log");
    game.nick = document.getElementById("nickLog").value;
    setNick();
    goToMain();
    showGrid("grid-container_main");
}
function setMain() {
    hideGridAll();
    setBalance();
}
function goToMain() {
    game.view = View.Main;
    setMain();
}
// ~MAIN~
function getTab(s) {
    return document.getElementById(s);
}
function addCell(row, s) {
    let newCell = row.insertCell();
    newCell.appendChild(document.createTextNode(s));
}
function setCargoShipOnPlanet() {
    let p = game.planet.items;
    let s = game.ship.cargo;
    let tab = [];
    for (let name of p.keys()) {
        tab[name] = [p.get(name).available, 0];
    }
    for (let name of s.keys()) {
        if (typeof tab[name] === "undefined") {
            tab[name] = [0, s.get(name).quantity];
        }
        else {
            tab[name][1] = s.get(name).quantity;
        }
    }
    clearTableId("cargo_ship_planet");
    let tableToAdd = getTab("cargo_ship_planet");
    // tslint:disable-next-line: forin
    for (let k in tab) {
        let row = tableToAdd.insertRow();
        addCell(row, k);
        addCell(row, tab[k][0]);
        addCell(row, tab[k][1]);
    }
    for (let i = 2; i < tableToAdd.rows.length; i++) {
        let row = tableToAdd.rows[i];
        row.onclick = () => { selectItem(row.cells[0].textContent); };
    }
}
function availableItemsOnPlanet() {
    let p = game.planet.items;
    let s = game.ship.cargo;
    let tab = [];
    for (let name of p.keys()) {
        tab[name] = [1];
    }
    for (let name of s.keys()) {
        tab[name] = [1];
    }
    let availableItems = [];
    let i = 0;
    // tslint:disable-next-line: forin
    for (let k in tab) {
        availableItems[i] = k;
        i++;
    }
    return availableItems;
}
function setPriceShipOnPlanet() {
    let p = game.planet.items;
    let s = game.ship.cargo;
    let tab = [];
    p.forEach((v, k, m) => { tab[v.name] = [v.buy_price, v.sell_price]; });
    for (let name of s.keys()) {
        if (typeof tab[name] === "undefined") {
            tab[name] = [0, 0];
        }
    }
    clearTableId("price_ship_planet");
    let tableToAdd = getTab("price_ship_planet");
    // tslint:disable-next-line: forin
    for (let k in tab) {
        let row = tableToAdd.insertRow();
        addCell(row, k);
        addCell(row, tab[k][0]);
        addCell(row, tab[k][1]);
    }
    for (let i = 2; i < tableToAdd.rows.length; i++) {
        let row = tableToAdd.rows[i];
        row.onclick = () => { selectItem(row.cells[0].textContent); };
    }
}
function clearOptions(s) {
    let x = document.getElementById(s);
    while (x.length > 0) {
        x.remove(x.length - 1);
    }
}
function setTrade() {
    clearOptions("trade_item");
    let options = availableItemsOnPlanet();
    let x = document.getElementById("trade_item");
    // tslint:disable-next-line: forin
    for (let v in options) {
        // create new option element
        let opt = document.createElement("option");
        // create text node to add to option element (opt)
        opt.appendChild(document.createTextNode(options[v]));
        // set value property of opt
        opt.value = options[v];
        // add opt to end of select box (sel)
        x.appendChild(opt);
    }
}
function setFlyDestination() {
    clearOptions("destination");
    const x = document.getElementById("destination");
    for (let k of game.planets.keys()) {
        if (k === game.ship.planet_from) {
            continue;
        }
        // create new option element
        const opt = document.createElement("option");
        // create text node to add to option element (opt)
        opt.appendChild(document.createTextNode(k));
        // set value property of opt
        opt.value = k;
        // add opt to end of select box (sel)
        x.appendChild(opt);
    }
}
function setCargoSize() {
    const s = game.ship.cargo_size + " / " + game.ship.cargo_max_size;
    document.getElementById("capacity_ship_planet").textContent = s;
}
function setShipOnPlanet() {
    document.getElementsByClassName("ship_ship_planet")[0].textContent = game.planet.name;
    document.getElementsByClassName("planet_ship_planet")[0].textContent = game.ship.name;
    setBalance();
    setCargoShipOnPlanet();
    setPriceShipOnPlanet();
    setTrade();
    setFlyDestination();
    setCargoSize();
}
function custom_compare(a, b) {
    // I'm assuming all values are numbers
    return a[1] - b[1];
}
function genRank(tab) {
    while (tab.length < 5) {
        const nick = "GRACZ_" + Math.round((Math.random() * 1000)) % 100;
        const score = Math.round((Math.random() * 10000)) % 5000;
        tab.push([nick, score]);
    }
    return tab;
}
function setRankTab(tab) {
    clearTableId("rank");
    const tableToAdd = getTab("rank");
    for (let i = 0; i < tab.length; i++) {
        const row = tableToAdd.insertRow();
        addCell(row, "");
        addCell(row, tab[i][0]);
        addCell(row, tab[i][1].toString());
    }
}
function setRank() {
    let tab = [];
    for (let i = 0; i < localStorage.length; i++) {
        const k = localStorage.key(i);
        tab.push([k, parseInt(localStorage.getItem(k), 10)]);
    }
    if (tab.length < 5) {
        tab = genRank(tab);
    }
    tab.sort(custom_compare).reverse();
    setRankTab(tab);
}
window.onload = () => {
    game = new Game(JSON.parse(data));
    // localStorage.setItem("Czarna Pantera", "999");
    // localStorage.setItem("Czarna Wdowa", "333");
    if (typeof (Storage) !== "undefined") {
        setRank();
    }
    setNick();
    setBalance();
    setShipsMain();
    setPlanetsMain();
};
function updateCargoPlanet(name, onShip, onPlanet) {
    const table = getTab("cargo_ship_planet");
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < table.rows.length; i++) {
        const row = table.rows[i];
        if (row.cells[0].textContent === name) {
            row.cells[1].textContent = onPlanet.toString(10);
            row.cells[2].textContent = onShip.toString(10);
        }
    }
}
function buy(howMuch, itemName) {
    const availableSpace = game.ship.cargo_max_size - game.ship.cargo_size;
    const price = game.planet.getPriceBuy(itemName);
    const availableItems = game.planet.getItemQuantity(itemName);
    const canAford = Math.floor(game.balance / price);
    const quantityToBuy = Math.min(howMuch, availableSpace, canAford, availableItems);
    if (quantityToBuy === 0) {
        return;
    }
    game.planet.give(itemName, quantityToBuy);
    game.ship.get(itemName, quantityToBuy);
    game.ship.cargo_size += quantityToBuy;
    game.balance -= quantityToBuy * price;
}
function buyDisplay() {
    const itemName = document.getElementById("trade_item").value;
    const howMuch = parseInt(document.getElementById("trade_how").value, 10);
    buy(howMuch, itemName);
    setBalance();
    setCargoSize();
    updateCargoPlanet(itemName, game.ship.getItemQuantity(itemName), game.planet.getItemQuantity(itemName));
}
function sell(howMuch, itemName) {
    const price = game.planet.getPriceSell(itemName);
    const availableItems = game.ship.getItemQuantity(itemName);
    const quantityToSell = Math.min(howMuch, availableItems);
    if (quantityToSell === 0) {
        return;
    }
    game.balance += quantityToSell * price;
    game.planet.get(itemName, quantityToSell);
    game.ship.give(itemName, quantityToSell);
    game.ship.cargo_size -= quantityToSell;
}
function sellDisplay() {
    const itemName = document.getElementById("trade_item").value;
    const howMuch = parseInt(document.getElementById("trade_how").value, 10);
    sell(howMuch, itemName);
    setBalance();
    setCargoSize();
    updateCargoPlanet(itemName, game.ship.getItemQuantity(itemName), game.planet.getItemQuantity(itemName));
}
function end_game(int) {
    setBalance();
    showGrid("grid-container_end");
    clearInterval(int);
    hideGridAll();
    if (localStorage.length >= 5) {
        localStorage.clear();
    }
    if (localStorage.getItem(game.nick) === null || parseInt(localStorage.getItem(game.nick), 10) < game.balance) {
        localStorage.setItem(game.nick, game.balance.toString());
    }
}
function selectItem(item) {
    document.getElementById("trade_item").value = item;
}
function backToStart() {
    hideGrid("grid-container_log");
}
function goToLog() {
    showGrid("grid-container_log");
}
function newGame() {
    game = new Game(JSON.parse(data));
    if (typeof (Storage) !== "undefined") {
        setRank();
    }
    setNick();
    setBalance();
    setShipsMain();
    setPlanetsMain();
    hideGrid("grid-container_main");
    hideGrid("grid-container_end");
    showGrid("grid-container_start");
}
